# vDial-up client
# Copyright (C) 2015 - 2016 Nathaniel Olsen
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import operator as op
import math
import cmath
import ast
from multiprocessing import Process, Queue
from queue import Empty

operators = {ast.Add: op.add, ast.Sub: op.sub, ast.Mult: op.mul,
             ast.Div: op.truediv, ast.Pow: op.pow, ast.BitXor: op.xor,
             ast.USub: op.neg, ast.FloorDiv: op.ifloordiv, ast.Mod: op.mod, ast.BitOr: op.ior, ast.BitAnd: op.and_}

def eval_expr(q, expr):
    try:

        result = eval_(ast.parse(unbang(expr), mode='eval').body)
    except Exception as ex:
        result = ex
    q.put(result)

def sqrt(x):
    if isinstance(x, complex) or x < 0:
     return cmath.sqrt(x)
    else:
      return math.sqrt(x)

def unbang(s):
    """hack to rewrite N! notation with fac(N) notation, as we can't touch the ast."""
    subst = {}
    ins = defaultdict(str)
    parenOpenPos = []
    parenMatchPos = 0
    atomPosition = 0
    newAtom = False

    for i, c in enumerate(s):
        if c == '(':
            if not newAtom: # use start of func call
                parenOpenPos.append(atomPosition)
            else:
                parenOpenPos.append(i)
        elif c == ')':
            if len(parenOpenPos) == 0:
                raise ValueError('unmatched closing parenthesis')
            atomPosition = parenOpenPos.pop()
        elif c == '!':
            subst[i] = ')'
            ins[atomPosition] += 'fac('

        if not c.isalnum():
            newAtom = True
        elif newAtom:
            atomPosition = i
            newAtom = False

    s2 = []
    for i, c in enumerate(s):
        if i in ins:
            s2.append(ins[i])
        if i in subst:
            s2.append(subst[i])
        else:
            s2.append(c)

    return ''.join(s2)

calc_funcs = {
    'fac': math.factorial,
    'sin': math.sin,
    'max': max,
    'ln': math.log,
    'sqrt': sqrt,
    'cos': math.cos,
    'tan': math.tan,
    'atan': math.atan,
    'atan2': math.atan2,
    'asin': math.asin,
    'asinh': math.asinh,
    'exp': math.exp,
    'sinh': math.sinh,
    'log10': math.log10,
    'modf': math.modf,
    'log1p': math.log1p,
    'floor': math.floor,
    'abs': op.abs,
    'pow': math.pow,
    'round': round,
}

calc_consts = {
    'pi': math.pi,
    'e': math.e
}

def eval_(node):
    if isinstance(node, ast.Num): # <number>
        return node.n
    elif isinstance(node, ast.BinOp): # <left> <operator> <right>
        return operators[type(node.op)](eval_(node.left), eval_(node.right))
    elif isinstance(node, ast.UnaryOp): # <operator> <operand> e.g., -1
        return operators[type(node.op)](eval_(node.operand))
    elif isinstance(node, ast.Call): # name(arg0,arg1...)
        name = node.func.id
        if name not in calc_funcs:
            raise ValueError("function *%s* not defined" % name)
        func = calc_funcs[name]
        return func(*[eval_(x) for x in node.args])
    elif isinstance(node, ast.Name):
        name = node.id
        if name not in calc_consts:
            raise ValueError("constant *%s* not defined" % name)
        return calc_consts[name]
    else:
        raise TypeError(node)

def power(a, b):
  return a**b
operators[ast.Pow] = power

def eval(problem):
	try:
		q = Queue()
		p = Process(target=eval_expr, args=(q, problem,))
		p.start()
		try:
			result = q.get(timeout=1)
			p.join(1)
		except Empty:
			result = OverflowError("Error: Timeout.")
			p.terminate()
			p.join(1)
		if isinstance(result, Exception):
			raise result
			result = str(result)
    except OverflowError:
    	print("Error: Timeout.")

    except ZeroDivisionError:
    	print("Error: Divided by Zero.")

    except Exception:
    	print("Error: An error has occured.")