# vDial-up client
# Copyright (C) 2015 - 2016 Nathaniel Olsen
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from os import name, makedirs, path
from subprocess import call
from json import load, dump
from shutil import copyfile

if not path.isdir("apps"):
    makedirs("apps")




def saveconfig():
    if name == 'nt':
        dump(config, open('conf\config.json', 'w'), indent=2)
    else:
        dump(config, open('conf/config.json', 'w'), indent=2)
    return

if name == 'nt':
    try:
        config = load(open('conf\config.json'))
    except FileNotFoundError:
        copyfile('conf\config.json.example', 'conf\config.json')

else:
    try:
        config = load(open('conf/config.json'))
    except FileNotFoundError:
        copyfile('conf/config.json.example', 'conf/config.json')

port = int(5000) # Default port for vDial-up, DO NOT CHANGE THIS.
VerCheckPort = int(5001)
VerChkServer = '138.68.11.195'
RegServ_vNumber = '100000000'
#RegServ_IP = '138.68.11.195'
RegServ_IP = '127.0.0.1'
RegServ_MD5SUM = '839d9a51d9f2925d249bcb5fe37979c6'

def clear():
    if name == 'nt':
        call('cls')
    else:
        call('clear')

def dialupnoise():
    if config['Misc Settings']['Dial-up noise [Beta]'] == 'False':
        pass
    else:
        try:
            import simpleaudio as sa
        except ImportError:
            print('Error: Dial-up noise is unavailable. Did you forget to install "libasound2-dev" and run "pip install -r requirements.txt"?')
        if name == 'nt':
            wave_obj = sa.WaveObject.from_wave_file("libs\dialupnoise.wav")
            play_obj = wave_obj.play()
            play_obj.wait_done()

        else:
            wave_obj = sa.WaveObject.from_wave_file("libs/dialupnoise.wav")
            play_obj = wave_obj.play()
            play_obj.wait_done()
