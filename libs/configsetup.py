# vDial-up client
# Copyright (C) 2015 - 2016 Nathaniel Olsen
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from json import load, dump
from vDialupcore import clear
from time import sleep
from subprocess import call
from os import name
import sys

if name == 'nt':
    config = load(open('conf\config.json'))
else:
    config = load(open('conf/config.json'))

def setup():
	print("*Client setup*")
	config['Misc Settings']['Update check upon startup'] = input('Check updates upon startup? (True/False): ')
	config['skip_startscreen'] = input('Disable the startscreen? (True/False): ')
	config['Misc Settings']['Background update check'] = input('Check for updates in the background? (True/False): ')
	config['Misc Settings']['Background update frequency check'] = input('How frequently you want your client to check for updates in the background (in seconds, default is 600): ')
	config['Misc Settings']['Dial-up noise [Beta]'] = input('Disable the dial-up noise when dialing?\nIt may or may not work on your system (True/False): ')
	config['Misc Settings']['Hide notifications'] = input('Hide Notifications? (True/False): ')
	choice = input("Save settings? (In case of typo) (Y/N): ")
	if choice == 'Y' or 'y':
		config['firsttimerun'] = False
		dump(config, open('config.json', 'w'), indent=2)
		print('*Setup completed, restarting client in 3 seconds...*')
		sleep(3)
		call([sys.executable, os.path.join(get_script_dir(), 'client')])
	else:
		clear()
		setup()

setup()