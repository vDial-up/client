# vDial-up client
# Copyright (C) 2015 - 2016 Nathaniel Olsen
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from time import sleep
import socket
import libs.vDialupcore as core
from multiprocessing import Process
from json import load, dump
from os import name
import base64
import sys
import struct

def MD5SUM_mismatch(vNumber_to_connect, received, sock):
    print("*Warning: The MD5SUM on file does not match with the one on file, Do you wish to continue? (Y/N)")
    if vNumber_to_connect == core.RegServ_vNumber:
        MD5SUM_on_file = core.RegServ_MD5SUM
    else:
        pass # Right now, there is no way to retrieve a server's md5sum until I implement md5sum retriving in RegServ.
    print("MD5SUM on file: %s" % (MD5SUM_on_file))
    print("MD5SUM according to server: %s" % (received.split()[1]))
    print("")
    choice = input("Enter choice (Y/N): ")
    if choice == 'Y' or choice == 'y':
        init(received, sock, vNumber_to_connect)
    if choice == 'N' or choice == 'n':
        sys.exit() # Exit for now.

class main():
    def listen_for_data(sock):
        while 1:
            global received
            received = str(sock.recv(4096), 'utf-8')
        while not received:
            break
    def vdialing(vNumber_to_connect, vNumber_IP):
        print("vDialing %s..." % (vNumber_to_connect))
        core.dialupnoise()
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.connect((vNumber_IP, 5000))
        sock.sendall(bytes("SERVPING" + "\n", "utf-8"))
        received = str(sock.recv(4096), 'utf-8')
        Process(target=main.listen_for_data, args=[sock]).start()

        if received == "PONG":
            print("Connected.")
            sock.sendall(bytes("MD5SUMCHECK" + "\n", "utf-8"))
            if received.split()[0] == "MD5SUM:":
                if received.split()[1] == core.RegServ_MD5SUM:
                    print("MD5SUM verification was succeeded.")
                else:
                    MD5SUM_mismatch(vNumber_to_connect, received, sock)
            else:
                pass # Right now, there is no way to retrieve a server's md5sum until I implement md5sum retriving in RegServ.
            main.init(received, sock, vNumber_to_connect)

    def init(received, sock, vNumber_to_connect):
        sock.sendall(bytes("VNUMBER: " + core.config['vDial-up Settings']['vNumber'] + "\n", "utf-8"))
        if core.config['vDial-up Settings']['vNumber'] == "000000000":
           sock.sendall(bytes("CLIENTREGISTER" + "\n", "utf-8"))
           print(received)
           if received.split()[0] == "CONFIG:":
               if received.split()[1] == "vNumber":
                   core.config['vDial-up Settings']['vNumber'] = received.split()[2]
                   core.saveconfig()
               if received.split()[1] == "Key":
                   print(received)
                   core.config['vDial-up Settings']['Key'] = received.split()[2]
                   core.saveconfig()
               if received.split()[0] == "TOCLIENT:":
                   print(received)
                   print(" ".join(received.split()[2:]))
        else:
            sock.sendall(bytes("KEY: " + core.config['vDial-up Settings']['Key'] + "\n", "utf-8"))
            sock.sendall(bytes("INIT" + "\n", "utf-8"))
