# vDial-up client
##Warning: vDial-up is still in development, so some features might be missing, incomplete, or otherwise, not work at all. Use at your own risk.

IRC Channel: #vDial-up @ freenode

vDial-up is a virtual dial-up emulator that connects to the Internet, but emulates dial-up speeds.

Requirements: libasound2-dev, Python 3.

It's recommended to have a constant internet connection with a minimum speed of 100Kbps download/upload.

Copyright (C) 2015 - 2016 Nathaniel Olsen

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
